package com.ptit.garaoke.repository;

import com.ptit.garaoke.model.DonHang;
import com.ptit.garaoke.model.LinhKienDonHang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinhKienDonHangRepository extends JpaRepository<LinhKienDonHang, Integer> {
}
