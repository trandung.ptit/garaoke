package com.ptit.garaoke.repository;

import com.ptit.garaoke.model.DonHang;
import com.ptit.garaoke.model.LinhKien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinhKienRepository extends JpaRepository<LinhKien, Integer> {
}
