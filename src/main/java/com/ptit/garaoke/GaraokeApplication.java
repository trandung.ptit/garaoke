package com.ptit.garaoke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GaraokeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaraokeApplication.class, args);
    }

}
