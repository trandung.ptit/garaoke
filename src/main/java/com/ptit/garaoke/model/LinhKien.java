package com.ptit.garaoke.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Table(name = "tbl_linh_kien")
@Entity
public class LinhKien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String price;
}
