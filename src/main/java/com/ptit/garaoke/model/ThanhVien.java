package com.ptit.garaoke.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tbl_thanh_vien")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor
@Data
public class ThanhVien {
    @Id
    @GeneratedValue
    private Integer id;
    @Column
    private String name;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String phone;
}
