package com.ptit.garaoke.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Entity
@Data
@NoArgsConstructor
@Table(name = "tbl_linh_kien_don_hang")
public class LinhKienDonHang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column
    private Long price;

    @Column
    private Integer quantity;

    @Column
    private Integer linhkienId;

    @Column
    private Integer donhangId;
}
