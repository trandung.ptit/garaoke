package com.ptit.garaoke.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_nhan_vien")
@Data
@NoArgsConstructor
public class NhanVien extends ThanhVien{
    @Column
    private String position;
}
