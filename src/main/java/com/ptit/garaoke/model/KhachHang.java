package com.ptit.garaoke.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "tbl_khach_hang")
@Builder
@AllArgsConstructor
public class KhachHang {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column
    private String name;

    @Column
    private String phone;

    @Column
    private String address;

    @Column
    private String note;

    @OneToMany(mappedBy="khachHang")
    private List<DonHang> donHangList;
}
