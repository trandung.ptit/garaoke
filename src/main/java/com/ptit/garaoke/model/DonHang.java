package com.ptit.garaoke.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
@Entity
@Data
@NoArgsConstructor
@Table(name = "tbl_don_hang")
@Builder
@AllArgsConstructor
public class DonHang {
    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private Date createdDate;

    @Column
    private Long totalPrice;

    @Column
    private String status;

    @Column
    private String xeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="khach_hang_id", referencedColumnName = "id")
    private KhachHang khachHang;

    @Column
    private Integer phutrachkythuatId;

    @Column
    private Integer nhanvienkhoId;

    @Column
    private Integer nhanvienketoanId;


}
