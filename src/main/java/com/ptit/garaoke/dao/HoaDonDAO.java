package com.ptit.garaoke.dao;

import com.ptit.garaoke.model.DonHang;
import com.ptit.garaoke.repository.DonHangRepository;
import com.ptit.garaoke.repository.NhanVienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HoaDonDAO {
    @Autowired
    DonHangRepository donHangRepository;

    @Autowired
    NhanVienRepository nhanVienRepository;


    @GetMapping("/")
    public String profile(Model model){
        return "index";
    }

    @RequestMapping(value = "/search/{donhang-id}",method = RequestMethod.GET)
    public String searchHoaDon(Model model,@PathVariable("donhang-id") String donhangId){
        System.out.println(donhangId);
        List<DonHang> donHangList = new ArrayList<>();
        if (donhangId.isBlank()){
            donHangList = donHangRepository.findAll();
        } else {
            DonHang donHang = donHangRepository.findById(Integer.parseInt(donhangId)).orElse(null);
            if (donHang != null){
                donHangList.add(donHang);
            }
        }
        model.addAttribute("donHangList", donHangList);
        return "search";
    }

    @GetMapping("/export")
    public String xuatKho(Model model,@ModelAttribute(value = "donHang") DonHang donHang){
        return "export";
    }

    @GetMapping("/404")
    public String notFound(){
        return "404";
    }

    @GetMapping("/check")
    public String check(){
        System.err.println(nhanVienRepository.findById(1).get().getName());
        donHangRepository.findAll().stream().forEach(System.out::println);
        return "404";
    }
}
